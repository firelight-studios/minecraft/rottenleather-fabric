package org.firelight.rottenleather.rottenleather;

import org.firelight.rottenleather.rottenleather.common.RottenLeatherItems;
import org.firelight.rottenleather.rottenleather.common.RottenLeatherLootModifiers;
import net.fabricmc.api.ModInitializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RottenLeather implements ModInitializer {
    public static final String MODID = "rottenleather";
    public static RottenLeather instance;
    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public void onInitialize() {
        instance = this;

        RottenLeatherItems.registerModItems();
        RottenLeatherLootModifiers.modifyLootTables();
    }
}
