package org.firelight.rottenleather.rottenleather.common;

import org.firelight.rottenleather.rottenleather.RottenLeather;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class RottenLeatherItems {

    public static final Item ROTTEN_CHUNK = registerItem("rotten_chunk",
            new Item(new FabricItemSettings()
                    .food(new FoodComponent.Builder().hunger(8).saturationModifier(0.1f)
                            .statusEffect(new StatusEffectInstance(StatusEffects.HUNGER, 600, 0), 0.8F)
                            .meat()
                            .build()).group(ItemGroup.FOOD)));

    public static final Item SWEETENED_CHUNK = registerItem("sweetened_chunk",
            new Item(new FabricItemSettings()
                    .food(new FoodComponent.Builder().hunger(8).saturationModifier(0.3F)
                            .statusEffect(new StatusEffectInstance(StatusEffects.HUNGER, 600, 0), 0.4F)
                            .meat()
                            .build()).group(ItemGroup.FOOD)));

    public static final Item FLESH_JERKY = registerItem("flesh_jerky",
            new Item(new FabricItemSettings()
                    .food(new FoodComponent.Builder().hunger(8).saturationModifier(0.5F).meat().build()).group(ItemGroup.FOOD)));

    private static Item registerItem(String name, Item item) {
        return Registry.register(Registry.ITEM, new Identifier(RottenLeather.MODID, name), item);
    }

    public static void registerModItems() {

    }
}
